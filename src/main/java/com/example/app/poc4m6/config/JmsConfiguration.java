package com.example.app.poc4m6.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.web.client.RestTemplate;

import javax.jms.ConnectionFactory;

/**
 * Jms Configuration
 */
@Configuration
@EnableJms
public class JmsConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(JmsConfiguration.class);

    /**
     * Creates a JmsListenerContainerFactory bean
     * @param connectionFactory the connection factory
     * @param configurer the default listener
     * @return the jms listener container factory bean
     */
    @Bean
    public JmsListenerContainerFactory<?> myFactory(final ConnectionFactory connectionFactory,
                                                    final DefaultJmsListenerContainerFactoryConfigurer configurer) {
        LOGGER.info("JmsConfiguration - init");
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        // This provides all boot's default to this factory, including the message converter
        configurer.configure(factory, connectionFactory);

        // You could still override some of Boot's default if necessary.
        LOGGER.info("JmsConfiguration - end - created factory: {}", factory);
        return factory;
    }

    /**
     * Return a rest template
     * @return {@link RestTemplate} rest template instance
     */
    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

}
