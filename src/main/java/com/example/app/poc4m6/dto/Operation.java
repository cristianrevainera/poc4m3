package com.example.app.poc4m6.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Operation type
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum Operation {
    INSERT("insert"),
    UPDATE("update"),
    DELETE("delete");

    private String operation;

}
