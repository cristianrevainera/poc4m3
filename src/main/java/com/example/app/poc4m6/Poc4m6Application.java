package com.example.app.poc4m6;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

/**
 * Aplication main class
 */
@SpringBootApplication
@EnableJms
public class Poc4m6Application {
/**
 * Application main method
  * @param args command line methods
 */
public static void main(final String[] args) {
SpringApplication.run(Poc4m6Application.class, args);
}
}
