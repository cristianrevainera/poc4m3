package com.example.app.poc4m6.services;

import com.example.app.poc4m6.dto.OrderDTO;
import com.example.app.poc4m6.dto.Operation;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


/**
 * Receiver Service
 */
@Service
public class ReceiverService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReceiverService.class);

    @Value("${poc4m1.url}")
    private String poc4m1Url;

    @Value("${poc4m7.url}")
    private String poc4m7Url;

    private RestTemplate restTemplate;

    /**
     * Receiver Service constructor
     * @param restTemplate {@link RestTemplate} rest template
     */
    @Autowired
    public ReceiverService(final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    /**
     * Recieves messages from activemq
     * @param strMessage {@link String} message from the queue
     */
    @JmsListener(destination = "jms.message.queue1")
    public void receiveMessage(final String strMessage)  {

        OrderDTO orderDTO = null;
        try {

            orderDTO = new ObjectMapper().readValue(strMessage, OrderDTO.class);

            if (Operation.INSERT.name().equalsIgnoreCase(orderDTO.getOperation())) {
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);

                HttpEntity<OrderDTO> requestEntity = new HttpEntity<OrderDTO>(orderDTO, headers);
                ResponseEntity<String> responseEntity
                            = restTemplate.postForEntity(poc4m1Url + "/orders", requestEntity, String.class);
                OrderDTO orderDTOReturned =
                        new ObjectMapper().readValue(responseEntity.getBody(), OrderDTO.class);
                orderDTO.setId(orderDTOReturned.getId());

            } else if (Operation.DELETE.name().equalsIgnoreCase(orderDTO.getOperation())) {

                Map<String, String> params = new HashMap<>();
                params.put("id", orderDTO.getId().toString());

                restTemplate.delete(poc4m1Url + "/orders/{id}", params);
            }

            Map<String, String> statusMap = new HashMap<>();
            statusMap.put("insert", "inserted");
            statusMap.put("delete", "deleted");

            HttpHeaders headersUrlEnc = new HttpHeaders();
            headersUrlEnc.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

            MultiValueMap<String, String> formData = new LinkedMultiValueMap<String, String>();
            formData.add("orderId", orderDTO.getId().toString());
            formData.add("status", statusMap.get(orderDTO.getOperation()));

            HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<MultiValueMap<String, String>>(formData, headersUrlEnc);

            restTemplate.postForEntity(poc4m7Url + "/order", requestEntity, String.class);

        } catch (IOException e) {
            LOGGER.info("ReceiverService#receiveMessage: error haciendo unmarshaling de order. ", e.getMessage());
        }
    }
}
