package com.example.app.poc4m6.services;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.internal.util.reflection.Whitebox;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


public class ReceiverTests {

    private final String jsonInsert =
            "{\"email\":\"test1@gmail.com\", \"operation\": \"insert\"}";

    private final String jsonInserted =
            "{\"id\":\"1\", \"email\":\"test1@gmail.com\", \"operation\": \"insert\"}";

    private final String jsonDelete = "{\"id\": 1, \"operation\": \"delete\"}";

    private RestTemplate restTemplateMock;
    private ReceiverService receiverService;

    private final String host1 = "http://localhost:8081";

    private final String host7 = "http://localhost:8087";

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setup() {
        restTemplateMock = mock(RestTemplate.class);
        receiverService  = new ReceiverService(restTemplateMock);

        Whitebox.setInternalState(receiverService, "poc4m1Url", host1);
        Whitebox.setInternalState(receiverService, "poc4m7Url", host7);
    }

    @Test
    public void receiveInsertMessage() throws IOException {

        ResponseEntity resEntityInserted = new ResponseEntity<String>(jsonInserted, HttpStatus.OK);

        doReturn(resEntityInserted).when(restTemplateMock).postForEntity(eq(host1 + "/orders"), any(HttpEntity.class), any());

        ResponseEntity responseEntityMock =  mock(ResponseEntity.class);
        doReturn(responseEntityMock).when(restTemplateMock).postForEntity(eq(host7 + "/order"), any(HttpEntity.class), any());

        receiverService.receiveMessage(jsonInsert);

        verify(restTemplateMock, times(1)).postForEntity(eq(host1 + "/orders"), any(HttpEntity.class), any());
        verify(restTemplateMock, times(1)).postForEntity(eq(host7 + "/order"), any(HttpEntity.class), any());

     }

    @Test
    public void receiveDeleteMessage() {

        ResponseEntity responseEntityMock =  mock(ResponseEntity.class);
        doReturn(responseEntityMock).when(restTemplateMock).postForEntity(eq(host7 + "/order"), any(HttpEntity.class), any());

        receiverService.receiveMessage(jsonDelete);

        verify(restTemplateMock, times(1)).postForEntity(eq(host7 + "/order"), any(HttpEntity.class), any());

    }

//    @Test
//    public void throwsIOException() {
//        receiverService.receiveMessage("{asdf}");
//    }

}
